# sale-webapp

The repository for the web application of Sale Webapp. Created for one of requirement to applying jobs in Qtasnim.

## Installation intructions

- Open the terminal  and navigate to your desired directory.

## Frontend

The  frontend of the application is built using React, Tailwind.

## Backend

The  backend of the application is built using PHP, Laravel and database using MySQL.
