<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockGoods extends Model
{
    use HasFactory;

    /**
     * excluded field to mass assignment
     */
    protected $guarded = [];
}
