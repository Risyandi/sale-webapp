<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Goods extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * excluded field to mass assignment
     */
    protected $guarded = [];

    /**
     * relations with stock goods
     */
    public function stockGoods()
    {
        return $this->hasOne(StockGoods::class, 'id');
    }

    /**
     * relations with type goods
     */
    public function typeGoods()
    {
        return $this->hasOne(TypeGoods::class, 'id');
    }
}
