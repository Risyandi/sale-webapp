<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionGoods extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * excluded field to mass assignment
     */
    protected $guarded = [];

    /**
     * get the goods that own transaction. Aiming for table relations
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id')->withTrashed();
    }

    /**
     * relations with stock goods
     */
    public function stockGoods()
    {
        return $this->belongsTo(StockGoods::class, 'stock_goods_id');
    }

    /**
     * relations with type goods
     */
    public function typeGoods()
    {
        return $this->belongsTo(TypeGoods::class, 'type_goods_id');
    }
}
