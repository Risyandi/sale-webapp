<?php

namespace App\Http\Controllers;

use App\Models\TypeGoods;
use Illuminate\Http\Request;

class TypeGoodsController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $typeGoods = TypeGoods::paginate(10);
            return response()->json($typeGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list type goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $typeGoods = TypeGoods::findOrFail($id);
            return response()->json($typeGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id type goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $typeGoods = TypeGoods::create($request->all());
            return response()->json($typeGoods, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created type goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['typeGoods'] as $postData) {
                TypeGoods::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $typeGoods = TypeGoods::findOrFail($id);
            $typeGoods->update($request->all());
            return response()->json($typeGoods);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated type goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $typeGoods = TypeGoods::findOrFail($id);
            $typeGoods->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * searching data in database with 
     * context: column_name, column_name 
     */
    public function search(Request $request)
    {
    }
}
