<?php

namespace App\Http\Controllers;

use App\Models\TransactionGoods;
use Illuminate\Http\Request;

class TransactionGoodsController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $transactionGoods = TransactionGoods::with('goods', 'stockGoods', 'typeGoods')->paginate(10);
            return response()->json($transactionGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list transaction goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $transactionGoods = TransactionGoods::with('goods')->findOrFail($id);
            return response()->json($transactionGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id transaction goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $transactionGoods = TransactionGoods::create($request->all());
            return response()->json($transactionGoods, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created transaction goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['transactionGoods'] as $postData) {
                TransactionGoods::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $transactionGoods = TransactionGoods::findOrFail($id);
            $transactionGoods->update($request->all());
            return response()->json($transactionGoods);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated transaction goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $transactionGoods = TransactionGoods::findOrFail($id);
            $transactionGoods->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * searching data in database with 
     * context: column_name, column_name 
     */
    public function search(Request $request)
    {
    }
}
