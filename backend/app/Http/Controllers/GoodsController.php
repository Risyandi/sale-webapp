<?php

namespace App\Http\Controllers;

use App\Models\Goods;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $goods = Goods::with('stockGoods', 'typeGoods')->paginate(10);
            return response()->json($goods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $goods = Goods::with('stockGoods', 'typeGoods')->findOrFail($id);
            return response()->json($goods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $goods = Goods::create($request->all());
            return response()->json($goods, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['goods'] as $postData) {
                Goods::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $goods = Goods::findOrFail($id);
            $goods->update($request->all());
            return response()->json($goods);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $goods = Goods::findOrFail($id);
            $goods->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * searching data in database with 
     * context: column_name, column_name 
     */
    public function search(Request $request)
    {
    }
}
