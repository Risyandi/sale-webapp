<?php

namespace App\Http\Controllers;

use App\Models\StockGoods;
use Illuminate\Http\Request;

class StockGoodsController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $stockGoods = StockGoods::paginate(10);
            return response()->json($stockGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list stock goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $stockGoods = StockGoods::findOrFail($id);
            return response()->json($stockGoods, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id stock goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $stockGoods = StockGoods::create($request->all());
            return response()->json($stockGoods, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created stock goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['stockGoods'] as $postData) {
                StockGoods::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $stockGoods = StockGoods::findOrFail($id);
            $stockGoods->update($request->all());
            return response()->json($stockGoods);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated stock goods', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $stockGoods = StockGoods::findOrFail($id);
            $stockGoods->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted career', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * searching data in database with 
     * context: column_name, column_name 
     */
    public function search(Request $request)
    {
    }
}
