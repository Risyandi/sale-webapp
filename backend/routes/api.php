<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\GoodsController;
use App\Http\Controllers\StockGoodsController;
use App\Http\Controllers\TransactionGoodsController;
use App\Http\Controllers\TypeGoodsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * endpoint group api transaction
 */
Route::prefix('transaction-goods')->group(function () {
    Route::get('/search', [TransactionGoodsController::class, 'search']);
    Route::get('/', [TransactionGoodsController::class, 'index']);
    Route::get('/{id}', [TransactionGoodsController::class, 'show']);
    Route::post('/', [TransactionGoodsController::class, 'store']);
    Route::post('/mass', [TransactionGoodsController::class, 'storeMultiple']);
    Route::put('/{id}', [TransactionGoodsController::class, 'update']);
    Route::delete('/{id}', [TransactionGoodsController::class, 'destroy']);
});

/**
 * endpoint group api goods
 */
Route::prefix('goods')->group(function () {
    Route::get('/search', [GoodsController::class, 'search']);
    Route::get('/', [GoodsController::class, 'index']);
    Route::get('/{id}', [GoodsController::class, 'show']);
    Route::post('/', [GoodsController::class, 'store']);
    Route::post('/mass', [GoodsController::class, 'storeMultiple']);
    Route::put('/{id}', [GoodsController::class, 'update']);
    Route::delete('/{id}', [GoodsController::class, 'destroy']);
});

/**
 * endpoint group api stock goods
 */
Route::prefix('stock-goods')->group(function () {
    Route::get('/search', [StockGoodsController::class, 'search']);
    Route::get('/', [StockGoodsController::class, 'index']);
    Route::get('/{id}', [StockGoodsController::class, 'show']);
    Route::post('/', [StockGoodsController::class, 'store']);
    Route::post('/mass', [StockGoodsController::class, 'storeMultiple']);
    Route::put('/{id}', [StockGoodsController::class, 'update']);
    Route::delete('/{id}', [StockGoodsController::class, 'destroy']);
});

/**
 * endpoint group api type goods
 */
Route::prefix('type-goods')->group(function () {
    Route::get('/search', [TypeGoodsController::class, 'search']);
    Route::get('/', [TypeGoodsController::class, 'index']);
    Route::get('/{id}', [TypeGoodsController::class, 'show']);
    Route::post('/', [TypeGoodsController::class, 'store']);
    Route::post('/mass', [TypeGoodsController::class, 'storeMultiple']);
    Route::put('/{id}', [TypeGoodsController::class, 'update']);
    Route::delete('/{id}', [TypeGoodsController::class, 'destroy']);
});
