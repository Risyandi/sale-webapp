<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_goods', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('goods_id')->nullable();
            $table->bigInteger('stock_goods_id')->nullable();
            $table->bigInteger('type_goods_id')->nullable();
            $table->bigInteger('total_sell_goods')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_goods');
    }
}
