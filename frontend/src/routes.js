import { lazy } from "react";

const Home = lazy(() => import("./pages/Home"));
const Details = lazy(() => import("./pages/Details"));
const Apply = lazy(() => import("./pages/Apply"));
const Dashboard = lazy(() => import("./pages/Admin/Auth/Dashboard"));
const Login = lazy(() => import("./pages/Admin/Auth/Login"));
const Register = lazy(() => import("./pages/Admin/Auth/Register"));
const Candidates = lazy(() => import("./pages/Admin/Auth/Candidates"));
const DetailsCandidates = lazy(() =>
  import("./pages/Admin/Auth/Candidates/Details")
);
const CareersForm = lazy(() => import("./pages/Admin/Auth/CareersForm"));
const DetailsCareers = lazy(() =>
  import("./pages/Admin/Auth/Dashboard/DetailsCareers")
);

const routes = [
  // if not found redirect to homepage or another page
  {
    path: "*",
    restricted: false,
    component: Home, // change this element to 404 page for the example
  },
  {
    path: "/",
    restricted: false,
    component: Home,
  },
  {
    path: "/careers-details/:id",
    restricted: false,
    component: Details,
  },
  {
    path: "/apply/:id/:title",
    restricted: false,
    component: Apply,
  },
  {
    path: "/admin/login",
    restricted: false,
    component: Login,
  },
  {
    path: "/admin/register",
    restricted: false,
    component: Register,
  },
  {
    path: "/admin/dashboard",
    restricted: true,
    component: Dashboard,
  },
  {
    path: "/admin/dashboard/details-careers/:id",
    restricted: true,
    component: DetailsCareers,
  },
  {
    path: "/admin/candidates",
    restricted: true,
    component: Candidates,
  },
  {
    path: "/admin/candidates/details/:id",
    restricted: true,
    component: DetailsCandidates,
  },
  {
    path: "/admin/careers/:id",
    restricted: true,
    component: CareersForm,
  },
];

export default routes;
