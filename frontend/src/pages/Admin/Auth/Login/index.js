import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { loginAdmin } from "../../../../api/servicesCareer";
import { storage, logout } from "../../../../utils";
import Swal from 'sweetalert2';

const LoginPage = () => {
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({
    email: "",
    password: "",
  });

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // check validation error
    let lengthErrObjValidation = Object.keys(Validation(values)).length;
    if (lengthErrObjValidation !== 0) {
      setErrors(Validation(values));
    } else {
      setErrors({}); // clear error previously
      loginAdmin(values)
        .then((value) => {
          if (value.success !== false) {
            const userLoginToken = value.token;
            storage.set("token-sale", userLoginToken); // store token to localstorage
            setTimeout(() => {
              window.location.href = "/admin/dashboard";
            }, 1000);
          } else {
            Swal.fire({
              title: 'Information',
              text: value.data.password[0],
              icon: 'error',
              confirmButtonColor: '#16A085',
            });
          }
        })
        .catch((e) => {
          Swal.fire({
            title: 'Information',
            text: e.response.data.message,
            icon: 'error',
            confirmButtonColor: '#16A085',
          });
        });
    }
  };

  const Validation = (values) => {
    const errors = {};

    const email_pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    const password_patteen =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;

    if (values.email === "") {
      errors.email = "Email is Required";
    } else if (!email_pattern.test(values.email)) {
      errors.email = "It should be a valid email address!";
    }

    if (values.password === "") {
      errors.password = "Password is Required";
    } else if (!password_patteen.test(values.password)) {
      errors.password =
        "Password should be 6-20 characters and include at least 1 letter, 1 number and 1 special character!";
    }

    return errors;
  };

  useEffect(() => {
    logout();
  // eslint-disable-next-line
  }, []);
  
  return (
    <>
      <div className="flex flex-col items-center">
        <div className="w-full flex flex-col justify-center items-center px-16 py-16 lg:w-1/2">
          <form
            className="flex flex-col items-center w-full"
            onSubmit={handleSubmit}>
            <label className="block mb-5 text-3xl lg:text-4xl font-medium text-black ">
              Sign In
            </label>
            <div className="w-full mb-6">
              <label className="block mb-2 text-md font-normal text-black ">
                Email
              </label>
              <input
                className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                name="email"
                onChange={onChange}
                required
              />
              {errors.email && <p className="text-red-600">{errors.email}</p>}
            </div>
            <div className="w-full mb-6">
              <label className="block mb-2 text-md font-normal text-black ">
                Password
              </label>
              <input
                type="password"
                className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                name="password"
                onChange={onChange}
                required
              />
              {errors.password && (
                <p className="text-red-600">{errors.password}</p>
              )}
            </div>
            <button className="font-extrabold text-white bg-green-sale py-3 px-20 lg:px-32 rounded-full cursor-pointer hover:bg-yellow-sale hover:text-green-sale">
              Login
            </button>
          </form>
          <p className="text-sm mt-5">
            New Account?{" "}
            <span className="font-semibold text-green-sale">
              <Link to={`/admin/register`}>Sign Up</Link>
            </span>
          </p>
        </div>
      </div>
    </>
  );
};
export default LoginPage;
