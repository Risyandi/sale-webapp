import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { createCareer, getCareer, updateCareer, getUsersDetail } from "../../../../api/servicesCareer";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import SideBar from "../../../../components/SideBar";
import { SyncLoader } from "react-spinners";
import { getToken, logout } from "../../../../utils";

const CareersForm = () => {
  const params = useParams();
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({
    title: "",
    department: "",
    description: "",
    location: "",
    level_experience: "",
  });
  const [users, setUsers] = useState({ name: '' });
  const tokenValue = getToken();

  const fetchUsers = () => {
    let formdataToken = { token: tokenValue };
    getUsersDetail(formdataToken)
      .then((result) => {
        setUsers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleValues = (e) => {
    setValues((prevState) => ({ ...prevState, description: e }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (params.id === "new") {
      // new career
      createCareer(values)
        .then((value) => {
          if (value.succes !== false) {
            setTimeout(() => {
              window.location.href = "/admin/dashboard";
            }, 1000);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      // update career
      updateCareer(params.id, values).then((value) => {
        if (value.succes !== false) {
          setTimeout(() => {
            window.location.href = "/admin/dashboard";
          }, 1000);
        }
      });
    }
  };

  useEffect(() => {
    fetchUsers();

    // new career
    if (params.id === "new") {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
      return;
    } else {
      // get data career by id
      getCareer(params.id)
        .then((result) => {
          setValues(result);
        })
        .catch((e) => {
          console.log(e);
        });
    }

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  // eslint-disable-next-line
  }, [params.id]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col items-center h-screen pt-20">
          <SyncLoader color={"#16A085"} loading={loading} size={10} />
        </div>
      ) : (
        <div>
          <div className="flex flex-col items-center py-5 border-b">
            <h1 className="text-3xl font-semibold">Welcome, {users.name}</h1>
            <p>Manage Careers For Kyoob</p>
          </div>
          <div className="flex">
            <SideBar />
            <div className="flex w-full border-l px-5 pb-10 lg:px-20">
              <div className="flex w-full flex-col mt-2">
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-2xl font-bold">
                    {params.id === "new" ? "Create Careers" : "Update Careers"}
                  </h1>
                </div>
                <div className=" flex flex-col mt-5">
                  <form onSubmit={handleSubmit} className="items-centerw-full">
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-5">
                      <div className="w-full">
                        <label className="block mb-2 text-md font-normal text-black ">
                          Title
                        </label>
                        <input
                          onChange={onChange}
                          className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                          name="title"
                          type="text"
                          value={values.title}
                          required
                        />
                      </div>
                      <div className="w-full mb-6">
                        <label className="block mb-2 text-md font-normal text-black ">
                          Department
                        </label>
                        <input
                          onChange={onChange}
                          className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                          name="department"
                          type="text"
                          value={values.department}
                          required
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-1">
                      <div className="w-full mb-6">
                        <label className="block mb-2 text-md font-normal text-black ">
                          Description
                        </label>
                        <ReactQuill
                          onChange={(e) => handleValues(e)}
                          theme="snow"
                          className="h-64"
                          value={values.description}
                          formats={"formats"}
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-5 mt-16 lg:mt-10">
                      <div className="w-full">
                        <label className="block mb-2 text-md font-normal text-black ">
                          Location
                        </label>
                        <input
                          onChange={onChange}
                          className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                          name="location"
                          type="text"
                          value={values.location}
                          required
                        />
                      </div>
                      <div className="w-full mb-6">
                        <label className="block mb-2 text-md font-normal text-black ">
                          Level Experience
                        </label>
                        <input
                          onChange={onChange}
                          className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-2 focus:outline-none"
                          name="level_experience"
                          type="text"
                          value={values.level_experience}
                          required
                        />
                      </div>
                    </div>
                    <button className="font-extrabold text-white bg-green-sale py-3 px-20 lg:px-32 rounded-full cursor-pointer hover:bg-yellow-sale hover:text-green-sale">
                      {params.id === "new"
                        ? "Create Careers"
                        : "Update Careers"}
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CareersForm;
