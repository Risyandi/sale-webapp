import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { getToken, logout } from "../../../../utils";
import {
  deleteCareer,
  getCareersList,
  getUsersDetail,
} from "../../../../api/servicesCareer";
import { SyncLoader } from "react-spinners";
import SideBar from "../../../../components/SideBar";
import Pagination from "../../../../components/Pagination";
import Swal from "sweetalert2";

const Dashboard = () => {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState({ name: "" });
  const [careers, setCareers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [lastPage, setLastPage] = useState(0);
  const [totalPosition, setTotalPosition] = useState(0);
  const navigate = useNavigate();
  const tokenValue = getToken();

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePrevPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const fetchUsers = () => {
    let formdataToken = { token: tokenValue };
    getUsersDetail(formdataToken)
      .then((result) => {
        setUsers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  const fetchCareersData = () => {
    getCareersList(currentPage)
      .then((result) => {
        setTotalPosition(result.total);
        setLastPage(result.last_page);
        setCareers(result.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteCareers = (id) => {
    Swal.fire({
      title: "Deleted Data",
      text: "Would you like to delete career?",
      icon: "warning",
      confirmButtonColor: "#16A085",
      showCancelButton: true,
    }).then((value) => {
      if (value.isConfirmed) {
        deleteCareer(id)
          .then((res) => {
            // eslint-disable-next-line
            location.reload();
          })
          .catch((e) => {
            console.log(e);
          });
      }
    });
  };

  useEffect(() => {
    fetchUsers();
    fetchCareersData();

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
    // eslint-disable-next-line
  }, [currentPage]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col items-center h-screen pt-20">
          <SyncLoader color={"#16A085"} loading={loading} size={10} />
        </div>
      ) : (
        <div>
          <div className="flex flex-col items-center py-5 border-b">
            <h1 className="text-3xl font-semibold">Welcome, {users.name}</h1>
            <p>Manage Careers For Kyoob</p>
          </div>
          <div className="flex">
            <SideBar />
            <div className="flex w-full border-l px-5 pb-10 lg:px-20">
              <div className="flex w-full flex-col mt-2">
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-2xl font-bold">Careers</h1>
                  <button
                    onClick={() => navigate("/admin/careers/new")}
                    className="font-bold text-white bg-green-sale py-2 px-10 lg:px-10 rounded-full cursor-pointer hover:bg-yellow-sale hover:text-green-sale">
                    New Careers
                  </button>
                </div>
                <div className=" flex flex-col mt-5">
                  <div className="flex flex-row justify-between">
                    <h1 className="font-semibold">All Careers</h1>
                    <h1 className="font-semibold">
                      {totalPosition} <span>Career</span>
                    </h1>
                  </div>
                  {careers.length !== 0 ? (
                    careers.map((career, index) => {
                      return (
                        <div key={index}>
                          <div className="lg:hidden mt-5 bg-gray-100 rounded">
                            <div className="w-full p-2">
                              <div className="flex flex-row justify-between items-start mt-2 mb-3">
                                <Link
                                  to={`/admin/dashboard/details-careers/${career.id}`}>
                                  <h1 className="text-xl font-bold cursor-pointer">
                                    {career.title}
                                  </h1>
                                </Link>
                                <div className="flex flex-row gap-2">
                                  <button
                                    onClick={() => deleteCareers(career.id)}
                                    className="px-5 py-1 rounded-full text-red-600 text-xs border border-red-600 hover:bg-red-600 hover:text-white">
                                    Delete
                                  </button>
                                  <Link to={`/admin/careers/${career.id}`}>
                                    <button className="px-5 py-1 rounded-full text-white text-xs bg-green-sale">
                                      Edit
                                    </button>
                                  </Link>
                                </div>
                              </div>
                              <div className="flex flex-row justify-between font-light">
                                <p>{career.department}</p>
                                <p>{career.level_experience}</p>
                              </div>
                              <p>{career.location}</p>
                            </div>
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <div className="text-center lg:hidden">
                      No data careers available
                    </div>
                  )}
                </div>

                <div className="hidden lg:block mt-4">
                  <table className="w-full text-left">
                    <thead className="w-full bg-gray-100">
                      <tr>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Careers Title
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Department
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Level Experience
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Location
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Action
                        </th>
                      </tr>
                    </thead>
                    {careers.length !== 0 ? (
                      careers.map((career, index) => {
                        return (
                          <tbody key={index}>
                            <tr>
                              <td className="p-3 bg-white cursor-pointer hover:font-bold">
                                <Link
                                  to={`/admin/dashboard/details-careers/${career.id}`}>
                                  {career.title}
                                </Link>
                              </td>
                              <td className="p-3 bg-white">
                                {career.department}
                              </td>
                              <td className="p-3 bg-white">
                                {career.level_experience}
                              </td>
                              <td className="p-3 bg-white">
                                {career.location}
                              </td>
                              <td className="p-3 bg-white">
                                <div className="flex flex-row gap-3">
                                  <button
                                    onClick={() => deleteCareers(career.id)}
                                    className="px-5 py-1 rounded-full text-red-600 text-xs border border-red-600 hover:bg-red-600 hover:text-white">
                                    Delete
                                  </button>
                                  <Link to={`/admin/careers/${career.id}`}>
                                    <button className="px-5 py-1 rounded-full text-white text-xs bg-green-sale">
                                      Edit
                                    </button>
                                  </Link>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        );
                      })
                    ) : (
                      <tbody>
                        <tr>
                          <td colSpan={5} className="text-center p-3 bg-white">
                            No data careers available
                          </td>
                        </tr>
                      </tbody>
                    )}
                  </table>
                </div>
                <Pagination
                  currentPage={currentPage}
                  lastPage={lastPage}
                  onNextPage={handleNextPage}
                  onPrevPage={handlePrevPage}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Dashboard;
