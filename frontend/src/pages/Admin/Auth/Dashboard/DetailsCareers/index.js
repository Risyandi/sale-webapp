import { useEffect, useState } from "react";
import {
  deleteCareer,
  getCareer,
  getUsersDetail,
} from "../../../../../api/servicesCareer";
import { getToken, logout } from "../../../../../utils";
import { Link, useParams } from "react-router-dom";
import { SyncLoader } from "react-spinners";
import SideBar from "../../../../../components/SideBar";
import Swal from "sweetalert2";

const DetailsCareers = () => {
  const params = useParams();
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState({ name: "" });
  const [career, setCareers] = useState([]);
  const tokenValue = getToken();

  const fetchUsers = () => {
    let formdataToken = { token: tokenValue };
    getUsersDetail(formdataToken)
      .then((result) => {
        setUsers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  const fetchCareersData = () => {
    getCareer(params.id)
      .then((result) => {
        setCareers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/candidates";
          }, 1000);
        }
        console.log(err);
      });
  };

  const deleteCareers = (id) => {
    Swal.fire({
      title: "Deleted Data",
      text: "Would you like to delete career?",
      icon: "warning",
      confirmButtonColor: "#16A085",
      showCancelButton: true,
    }).then((value) => {
      if (value.isConfirmed) {
        deleteCareer(id)
          .then((res) => {
            // eslint-disable-next-line
            location.reload();
          })
          .catch((e) => {
            console.log(e);
          });
      }
    });
  };

  useEffect(() => {
    fetchUsers();
    fetchCareersData();

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
    // eslint-disable-next-line
  }, [params.id]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col items-center h-screen pt-20">
          <SyncLoader color={"#16A085"} loading={loading} size={10} />
        </div>
      ) : (
        <div>
          <div className="flex flex-col items-center py-5 border-b">
            <h1 className="text-3xl font-semibold">Welcome, {users.name}</h1>
            <p>Manage Careers For Kyoob</p>
          </div>
          <div className="flex">
            <SideBar />
            <div className="flex w-full border-l px-5 pb-10 lg:px-20">
              <div className="flex w-full flex-col mt-2">
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-2xl font-bold">Careers</h1>
                  <div className="flex flex-row gap-2">
                    <button
                      onClick={() => deleteCareers(career.id)}
                      className="px-5 py-1 rounded-full text-red-600 text-xs border border-red-600 hover:bg-red-600 hover:text-white">
                      Delete
                    </button>
                    <Link to={`/admin/careers/${career.id}`}>
                      <button className="px-5 py-1 rounded-full text-white text-xs bg-green-sale">
                        Edit
                      </button>
                    </Link>
                  </div>
                </div>
                <div className=" flex flex-col mt-5">
                  <div>
                    <div className="flex flex-row items-center justify-between border-t border-b py-2">
                      <div className="flex flex-col justify-between">
                        <h1 className="text-lg font-semibold">
                          {career.title}
                        </h1>
                      </div>
                    </div>
                    <h1 className="font-normal mt-5">Details Careers</h1>
                  </div>
                  <div>
                    <h1 className="text-2xl md:text-3xl font-semibold">
                      {career.title}
                    </h1>
                    <div className="flex flex-col lg:flex-row lg:justify-between my-5 gap-3 border-y py-5">
                      <div className="flex flex-row items-center gap-2">
                        <ion-icon name="briefcase-outline"></ion-icon>
                        <p className="font-normal text-gray-500">
                          Department{" "}
                          <span className="font-bold text-black">
                            {career.department}
                          </span>
                        </p>
                      </div>
                      <div className="flex flex-row items-center gap-2">
                        <ion-icon name="podium-outline"></ion-icon>
                        <p className="font-normal text-gray-500">
                          Level Experience{" "}
                          <span className="font-bold text-black">
                            {career.level_experience}
                          </span>
                        </p>
                      </div>
                      <div className="flex flex-row items-center gap-2">
                        <ion-icon name="location-outline"></ion-icon>
                        <p className="font-normal text-gray-500">
                          Location{" "}
                          <span className="font-bold text-black">
                            {career.location}
                          </span>
                        </p>
                      </div>
                    </div>
                    <div
                      className="text-md font-light content-careers px-5"
                      dangerouslySetInnerHTML={{ __html: career.description }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default DetailsCareers;
