import { useEffect, useState } from "react";
import {
  deleteCandidate,
  getCandidates,
  getUsersDetail,
} from "../../../../../api/servicesCareer";
import { getToken, logout } from "../../../../../utils";
import { Link, useParams } from "react-router-dom";
import { SyncLoader } from "react-spinners";
import SideBar from "../../../../../components/SideBar";
import Swal from "sweetalert2";

const Candidates = () => {
  const params = useParams();
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState({ name: "" });
  const [candidates, setCandidates] = useState([]);
  const [careers, setCareers] = useState([]);
  const tokenValue = getToken();
  const FILE_URL = "https://api.kyoob.id/services-career/storage/candidate";

  const fetchUsers = () => {
    let formdataToken = { token: tokenValue };
    getUsersDetail(formdataToken)
      .then((result) => {
        setUsers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  const fetchCandidatesData = () => {
    getCandidates(tokenValue, params.id)
      .then((result) => {
        setCandidates(result);
        setCareers(result.career);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/candidates";
          }, 1000);
        }
        console.log(err);
      });
  };

  const deleteCandidates = (id) => {
    Swal.fire({
      title: "Deleted Data",
      text: "Would you like to delete candidates?",
      icon: "warning",
      confirmButtonColor: "#16A085",
      showCancelButton: true,
    }).then((value) => {
      if (value.isConfirmed) {
        deleteCandidate(tokenValue, id)
          .then((res) => {
            // eslint-disable-next-line
            window.location.href = "/admin/candidates";
          })
          .catch((e) => {
            console.log(e);
          });
      }
    });
  };

  useEffect(() => {
    fetchUsers();
    fetchCandidatesData();

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
    // eslint-disable-next-line
  }, [params.id]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col items-center h-screen pt-20">
          <SyncLoader color={"#16A085"} loading={loading} size={10} />
        </div>
      ) : (
        <div>
          <div className="flex flex-col items-center py-5 border-b">
            <h1 className="text-3xl font-semibold">Welcome, {users.name}</h1>
            <p>Manage Careers For Kyoob</p>
          </div>
          <div className="flex">
            <SideBar />
            <div className="flex w-full border-l px-5 pb-10 lg:px-20">
              <div className="flex w-full flex-col mt-2">
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-2xl font-bold">Candidates</h1>
                </div>
                <div className=" flex flex-col mt-5">
                  <div>
                    <div className="flex flex-row items-center justify-between border-t border-b py-2">
                      <div className="flex flex-col justify-between">
                        <h1 className="text-2xl font-bold">
                          {candidates.full_name}
                        </h1>
                        <h1 className="text-lg">{careers.title}</h1>
                      </div>
                      <button
                        onClick={() => deleteCandidates(candidates.id)}
                        className="px-5 py-1 rounded-lg text-sm text-red-600 border border-red-600 hover:bg-red-600 hover:text-white">
                        Delete
                        <br />
                        Candidate
                      </button>
                    </div>
                    <h1 className="font-normal mt-5">Details Candidates</h1>
                    <div className="grid grid-cols-1 pr-5">
                      <div className="grid grid-cols-1 mt-5 border-b-2 border-t-2 py-2">
                        <div className="flex flex-col">
                          <h3 className="font-light">Full Name</h3>
                          <h1 className="text-md font-bold">
                            {candidates.full_name}
                          </h1>
                        </div>
                      </div>
                      <div className="grid grid-cols-1 mt-2 gap-2 pb-2">
                        <div className="flex flex-col border-b-2 pb-2">
                          <h3 className="font-light">Email</h3>
                          <h1 className="text-md font-bold">
                            {candidates.email}
                          </h1>
                        </div>
                        <div className="flex flex-col border-b-2 pb-2">
                          <h3 className="font-light">Phone Number</h3>
                          <h1 className="text-md font-bold">
                            {candidates.phone_number}
                          </h1>
                        </div>
                      </div>
                      <div className="grid grid-cols-2 border-b-2 py-2">
                        <div className="flex flex-col">
                          <h3 className="font-light">Location</h3>
                          <h1 className="text-md font-bold">
                            {candidates.location}
                          </h1>
                        </div>
                        <div className="flex flex-col items-end lg:items-start">
                          <h3 className="font-light">Website Url</h3>
                          <h1 className="text-md font-bold">
                            {candidates.website_url !== null ? candidates.website_url : '-'}
                          </h1>
                        </div>
                      </div>
                      <div className="grid grid-cols-2 border-b-2 py-2">
                        <div className="flex flex-col">
                          <h3 className="font-light">
                            Start Date Availability (Internships Program)
                          </h3>
                          <h1 className="text-md font-bold">
                            {candidates.start_date_availability_period !== null ? candidates.start_date_availability_period : '-'}
                          </h1>
                        </div>
                        <div className="flex flex-col items-end lg:items-start">
                          <h3 className="font-light">
                            End Date Availability (Internships Program)
                          </h3>
                          <h1 className="text-md font-bold">
                            {candidates.end_date_availability_period !== null ? candidates.end_date_availability_period : '-'}
                          </h1>
                        </div>
                      </div>
                      <div className="grid grid-cols-1 mt-2 gap-2 border-b-2 pb-2">
                        <div className="flex flex-col">
                          <h3 className="font-light">Source Info Career</h3>
                          <h1 className="text-md font-bold">
                            {candidates.source_info_career}
                          </h1>
                        </div>
                      </div>
                      <div className="grid grid-cols-2 border-b-2 py-2">
                        <div className="flex flex-col gap-2 pb-2">
                          {candidates.file_path_cv !== null ? (
                            <>
                              <h3 className="font-light">File CV</h3>
                              <Link
                                target="_blank"
                                to={`${FILE_URL}/${candidates.file_path_cv}`}>
                                <button className="px-5 py-1 rounded-lg text-sm text-green-sale border border-green-sale hover:bg-green-sale hover:text-white">
                                  View
                                </button>
                              </Link>
                            </>
                          ) : null}
                        </div>
                        <div className="flex flex-col items-end lg:items-start gap-2">
                          {candidates.file_path_portfolio !== null ? (
                            <>
                              <h3 className="font-light">File Portfolio</h3>
                              <Link
                                target="_blank"
                                to={`${FILE_URL}/${candidates.file_path_portfolio}`}>
                                <button className="px-5 py-1 rounded-lg text-sm text-green-sale border border-green-sale hover:bg-green-sale hover:text-white">
                                  View
                                </button>
                              </Link>
                            </>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Candidates;
