import { useEffect, useState } from "react";
import {
  getCandidatesList,
  getUsersDetail,
} from "../../../../api/servicesCareer";
import { getToken, logout } from "../../../../utils";
import { Link } from "react-router-dom";
import { SyncLoader } from "react-spinners";
import SideBar from "../../../../components/SideBar";
import Pagination from "../../../../components/Pagination";
import moment from "moment";

const Candidates = () => {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState({ name: "" });
  const [candidates, setCandidates] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [lastPage, setLastPage] = useState(0);
  const tokenValue = getToken();

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePrevPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const fetchUsers = () => {
    let formdataToken = { token: tokenValue };
    getUsersDetail(formdataToken)
      .then((result) => {
        setUsers(result);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  const fetchCandidatesData = () => {
    getCandidatesList(tokenValue, currentPage)
      .then((result) => {
        setLastPage(result.last_page);
        setCandidates(result.data);
      })
      .catch((err) => {
        let status = err.response.status;
        if (status === 401) {
          logout();
          setTimeout(() => {
            window.location.href = "/admin/login";
          }, 1000);
        }
        console.log(err);
      });
  };

  useEffect(() => {
    fetchUsers();
    fetchCandidatesData();

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
    // eslint-disable-next-line
  }, [currentPage]);

  return (
    <>
      {loading ? (
        <div className="flex flex-col items-center h-screen pt-20">
          <SyncLoader color={"#16A085"} loading={loading} size={10} />
        </div>
      ) : (
        <div>
          <div className="flex flex-col items-center py-5 border-b">
            <h1 className="text-3xl font-semibold">Welcome, {users.name}</h1>
            <p>Manage Careers For Kyoob</p>
          </div>
          <div className="flex">
            <SideBar />
            <div className="flex w-full border-l px-5 pb-10 lg:px-20">
              <div className="flex w-full flex-col mt-2">
                <div className="flex flex-row items-center justify-between">
                  <h1 className="text-2xl font-bold">Candidates</h1>
                </div>
                <div className=" flex flex-col mt-5">
                  <div className="flex flex-row justify-between">
                    <h1 className="font-semibold">All Candidates</h1>
                  </div>
                  {candidates.length !== 0 ? (
                    candidates.map((candidates, index) => {
                      return (
                        <div key={index}>
                          <div className="lg:hidden mt-5 bg-gray-100 rounded">
                            <div className="w-full p-2">
                              <div className="flex flex-row justify-between items-start mt-2 mb-3">
                                <h1 className="text-xl font-bold">
                                  {candidates.full_name}
                                </h1>
                                <div className="flex flex-row gap-2">
                                  <Link
                                    to={`/admin/candidates/details/${candidates.id}`}>
                                    <button className="px-5 py-1 rounded-full text-white text-xs bg-green-sale">
                                      View
                                    </button>
                                  </Link>
                                </div>
                              </div>
                              <div className="flex flex-row justify-between font-light">
                                <div className="flex flex-col">
                                  <p>Positions</p>
                                  <p className="font-medium">
                                    {candidates.career.title}
                                  </p>
                                </div>
                                <div className="flex flex-col">
                                  <p className="text-end">
                                    Date of Application
                                  </p>
                                  <p className="font-medium text-end">
                                    {moment(candidates.created_at).format(
                                      "DD-MM-YYYY"
                                    )}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <div className="text-center lg:hidden">
                      No data candidates available
                    </div>
                  )}
                </div>

                <div className="hidden lg:block mt-4">
                  <table className="w-full text-left">
                    <thead className="w-full bg-gray-100">
                      <tr>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Name
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Position
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Date of Application
                        </th>
                        <th className="p-3 text-md font-semibold tracking-wide">
                          Action
                        </th>
                      </tr>
                      {candidates.length !== 0 ? (
                        candidates.map((candidates, index) => {
                          return (
                            <tr key={index}>
                              <td className="p-3 bg-white">
                                {candidates.full_name}
                              </td>
                              <td className="p-3 bg-white">
                                {candidates.career.title}
                              </td>
                              <td className="p-3 bg-white">
                                {moment(candidates.created_at).format(
                                  "DD-MM-YYYY"
                                )}
                              </td>
                              <td className="p-3 bg-white">
                                <Link
                                  to={`/admin/candidates/details/${candidates.id}`}>
                                  <button className="px-5 py-1 rounded-full text-white text-xs bg-green-sale">
                                    View
                                  </button>
                                </Link>
                              </td>
                            </tr>
                          );
                        })
                      ) : (
                        <td colSpan={4} className="text-center p-3 bg-white">
                          No data candidates available
                        </td>
                      )}
                    </thead>
                  </table>
                </div>

                <Pagination
                  currentPage={currentPage}
                  lastPage={lastPage}
                  onNextPage={handleNextPage}
                  onPrevPage={handlePrevPage}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Candidates;
