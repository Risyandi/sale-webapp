import { Link, useParams } from "react-router-dom";
import { getCareer } from "../../api/servicesCareer";
import { useEffect, useState } from "react";
import FormInput from "../../components/FormInput/FormInput";
import { inputCandidateProps } from "../../components/FormInput/InputProps";
import { applyCandidates } from "../../api/servicesCareer";
import Swal from "sweetalert2";

const Apply = () => {
  const params = useParams();
  const [career, setCareer] = useState([]);
  const [values, setValues] = useState({
    full_name: "",
    email: "",
    phone_number: "",
    location: "",
    website_url: "",
    source_info_career: "",
    start_date_availability_period: "",
    end_date_availability_period: "",
    file_path_cv: "",
    file_path_portfolio: null,
    career_id: params.id,
  });

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  function handleFile(e) {
    setValues({ ...values, [e.target.name]: e.target.files[0] });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    applyCandidates(values)
      .then((value) => {
        if (value.success !== false) {
          Swal.fire({
            title: "Thank you",
            text: "Your application successfully submitted",
            icon: "success",
            confirmButtonColor: "#16A085",
          }).then((value) => {
            if (value.isConfirmed || value.isDismissed) {
              setTimeout(() => {
                window.location.href = "/";
              }, 1000);
            }
          });
        } else {
          Swal.fire({
            title: "Information",
            text: "Please check again fill in the required information, and make sure the file format uploaded is (.pdf).",
            icon: "warning",
            confirmButtonColor: "#16A085",
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getCareer(params.id)
      .then((result) => {
        setCareer(result);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [params.id]);

  return (
    <>
      <div className="flex flex-col px-7 lg:px-64 py-10">
        <h1 className="text-3xl font-bold text-center">{career.title}</h1>
        <form
          onSubmit={handleSubmit}
          className="mt-10"
          encType="multipart/form-data">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-x-20">
            {inputCandidateProps.map((input) => (
              <FormInput
                key={input.id}
                {...input}
                value={values[input.name]}
                onChange={onChange}
              />
            ))}
          </div>

          <div className="grid grid-cols-1">
            <div className="mb-6">
              <label
                htmlFor="resume"
                className="block mb-2 text-md font-semibold text-black ">
                Curriculum Vitae (2MB Max){" "}
                <span className="text-red-600">*</span>
              </label>
              <div className="shadow-sm border border-gray-300 text-gray-900">
                <div className="p-3">
                  <input
                    onChange={handleFile}
                    type="file"
                    name="file_path_cv"
                    className="text-gray-900 text-md rounded-sm block w-full focus:outline-none"
                    required
                  />
                  <div className="text-gray-500 text-sm">
                    (Supported file formats: .pdf)
                  </div>
                </div>
              </div>
            </div>

            <div className="mb-6">
              <label
                htmlFor="resume"
                className="block mb-2 text-md font-semibold text-black ">
                Portofolio (Optional, 2MB Max)
              </label>
              <div className="shadow-sm border border-gray-300 block">
                <div className="p-3">
                  <input
                    onChange={handleFile}
                    type="file"
                    name="file_path_portfolio"
                    className="text-gray-900 text-md rounded-sm block w-full focus:outline-none"
                  />
                  <div className="text-gray-500 text-sm">
                    (Supported file formats: .pdf)
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="flex flex-col items-center">
            <div className="flex items-start mb-6">
              <div className="flex items-center h-5">
                <input
                  id="terms"
                  type="checkbox"
                  value=""
                  className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300"
                  required
                />
              </div>
              <label
                htmlFor="terms"
                className="ml-2 text-sm font-medium text-gray-900">
                I agree with the{" "}
                <Link to={`#`} className="text-blue-600">
                  terms and conditions
                </Link>
              </label>
            </div>
            <button className="font-extrabold text-white bg-green-sale py-3 px-20 lg:px-32 rounded-full cursor-pointer hover:bg-yellow-sale hover:text-green-sale">
              Submit
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Apply;
