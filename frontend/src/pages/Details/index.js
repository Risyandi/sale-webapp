import { Link, useParams } from "react-router-dom";
import { getCareer } from "../../api/servicesCareer";
import { useEffect, useState } from "react";
import { SyncLoader } from "react-spinners";

const DetailsPage = () => {
  const params = useParams();
  const [loading, setLoading] = useState(false);
  const [career, setCareer] = useState({ title: '', department: '', level_experience: '', location: '', description: '', id: '' });

  useEffect(() => {
    getCareer(params.id)
      .then((result) => {
        setCareer(result);
      })
      .catch((e) => {
        console.log(e);
      });

    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, [params.id]);

  const convertToDash = (str) => {
    /**
     * - String to lower case
     * - Create a regular expression to match all special characters and spaces
     * - Replace all special characters and spaces with a dash
     * - Return the output string
     */
    str = str.toLowerCase();
    const regex = /[^a-zA-Z0-9]+/g;
    const output = str.replace(regex, "-");
    return output;
  }

  return (
    <>
      <div className="flex flex-col px-7 py-10 md md:px-20 lg:px-72">
        {loading ? (
          <div className="flex flex-col items-center h-screen">
            <SyncLoader color={"#16A085"} loading={loading} size={10} />
          </div>
        ) : (
          <div>
            <h1 className="text-2xl md:text-3xl font-semibold">
              {career.title}
            </h1>
            <div className="flex flex-col lg:flex-row lg:justify-between my-5 gap-3 border-y py-5">
              <div className="flex flex-row items-center gap-2">
                <ion-icon name="briefcase-outline"></ion-icon>
                <p className="font-normal text-gray-500">
                  Department{" "}
                  <span className="font-bold text-black">
                    {career.department}
                  </span>
                </p>
              </div>
              <div className="flex flex-row items-center gap-2">
                <ion-icon name="podium-outline"></ion-icon>
                <p className="font-normal text-gray-500">
                  Level Experience{" "}
                  <span className="font-bold text-black">
                    {career.level_experience}
                  </span>
                </p>
              </div>
              <div className="flex flex-row items-center gap-2">
                <ion-icon name="location-outline"></ion-icon>
                <p className="font-normal text-gray-500">
                  Location{" "}
                  <span className="font-bold text-black">
                    {career.location}
                  </span>
                </p>
              </div>
            </div>
            <div
              className="text-md font-light content-careers"
              dangerouslySetInnerHTML={{ __html: career.description }}
            />
            <div className="flex flex-col items-center mt-10">
              <Link to={`/apply/${career.id}/${convertToDash(career.title)}`}>
                <button className="font-extrabold text-white bg-green-sale py-3 px-20 lg:px-32 rounded-full cursor-pointer hover:bg-yellow-sale hover:text-green-sale">
                  Apply Now
                </button>
              </Link>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailsPage;
