import { useEffect, useState, useCallback } from "react";
// import { getCareersList, searchCareer } from "../../api/servicesCareer";
import { getTransacGoodsList } from "../../api/servicesSale";
import CareerList from "../../components/CareerList";
import Pagination from "../../components/Pagination";

const HomePage = () => {
  const [careers, setCareers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [lastPage, setLastPage] = useState(0);
  const [totalPosition, setTotalPosition] = useState(0);
  const [search, setSearch] = useState("");
  const [notFoundContent, setNotFoundContent] = useState("");

  const handleInputChange = (event) => {
    setSearch(event.target.value);
  };

  const handleInputPress = async (e) => {
    let eventEnter = e.key;
    if (eventEnter === "Enter") {
      fetchSearch(search);
    }
  };

  const fetchSearch = () => {
    if (!!search && search !== "") {
      // searchCareer(search)
      //   .then((result) => {
      //     setTotalPosition(result.total);
      //     setLastPage(result.last_page);
      //     setCareers(result.data);
      //     if (result.data.length === 0) {
      //       setNotFoundContent("search");
      //     }
      //   })
      //   .catch((e) => {
      //     console.log(e);
      //   });
    } else {
      fetchCareersData();
    }
  };

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePrevPage = () => {
    setCurrentPage(currentPage - 1);
  };

  const fetchCareersData = () => {
    getTransacGoodsList(currentPage)
      .then((result) => {
        setTotalPosition(result.total);
        setLastPage(result.last_page);
        setCareers(result.data);
        if (result.data.length === 0) {
          setNotFoundContent("nocontent");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    fetchCareersData();
    // eslint-disable-next-line
  }, [currentPage]);

  // eslint-disable-next-line
  const contentNotFound = useCallback(() => {
    switch (notFoundContent) {
      case "nocontent":
        return "No more available position at the moment :(";
      case "search":
        return "We could't find anything :(";
      default:
        return "";
    }
  }, [notFoundContent]);

  return (
    <>
      <div className="flex flex-col pb-20 z-10 mt-0">
        <div className="flex flex-col px-7 lg:px-20">
          <div className="flex flex-row mt-10 justify-center items-center gap-5">
            <input
              type="text"
              id="search"
              className="bg-gray-50 border border-gray-300 text-black text-md rounded-2xl block w-full py-2.5 px-5 lg:w-1/2"
              placeholder="Search Transaction.."
              onChange={handleInputChange}
              onKeyUp={handleInputPress}
              value={search}
            />
            <button
              className="bg-green-sale text-white font-bold rounded-full py-2.5 px-5 lg:px-10"
              onClick={() => {
                fetchSearch();
              }}>
              Search
            </button>
          </div>
          <div className="flex flex-row items-center justify-between mt-12">
            <h1 className="text-xl lg:text-4xl font-semibold">
              Recent Transaction Sale
            </h1>
            <p className="text-xl font-bold">
              {totalPosition} <span className="font-normal">Transaction</span>
            </p>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-10 mt-12">
            {careers.map((career, index) => {
              return (
                <div key={index}>
                  <CareerList
                    id={career.id}
                    title={career.goods.name_goods}
                    department={career.type_goods.name_type_goods}
                    location={'-'}
                    level_experience={career.stock_goods.total_stock_goods}
                  />
                </div>
              );
            })}
          </div>

          {/* if not have content */}
          {careers.length === 0 ? (
            <div className="text-center p-5 text-xl lg:text-2xl">
              {contentNotFound()}
            </div>
          ) : null}

          {/* pagination */}
          <Pagination
            currentPage={currentPage}
            lastPage={lastPage}
            onNextPage={handleNextPage}
            onPrevPage={handlePrevPage}
          />
        </div>
      </div>
    </>
  );
};

export default HomePage;
