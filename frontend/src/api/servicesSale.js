import axios from "axios";

/**
 * stock goods services api
 */
export const getStockGoodsList = async (pages) => {
  const stockGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/stock-goods?page=${pages}`
  );
  return stockGoods.data;
};

export const getStockGoods = async (params) => {
  const stockGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/stock-goods/${params}`
  );
  return stockGoods.data;
};

export const createStockGoods = async (formdata) => {
  const stockGoods = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/stock-goods`,
    formdata
  );
  return stockGoods.data;
};

export const deleteStockGoods = async (params) => {
  const stockGoods = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/stock-goods/${params}`
  );
  return stockGoods.data;
};

export const updateStockGoods = async (id, formdata) => {
  const stockGoods = await axios.put(
    `${process.env.REACT_APP_BASE_URL}/api/v1/stock-goods/${id}`,
    formdata
  );
  return stockGoods.data;
};

/**
 * type goods services api
 */

export const getTypeGoodsList = async (pages) => {
  const typeGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/type-goods?page=${pages}`
  );
  return typeGoods.data;
};

export const getTypeGoods = async (id) => {
  const typeGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/type-goods/${id}`
  );
  return typeGoods.data;
};

export const createTypeGoods = async (formdata) => {
  const typeGoods = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/type-goods`,
    formdata
  );
  return typeGoods.data;
};

export const deleteTypeGoods = async (id) => {
  const typeGoods = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/type-goods/${id}`
  );
  return typeGoods.data;
};

export const updateTypeGoods = async (id, formdata) => {
  const typeGoods = await axios.put(
    `${process.env.REACT_APP_BASE_URL}/api/v1/type-goods/${id}`,
    formdata
  );
  return typeGoods.data;
};

/**
 * goods services api
 */

export const getGoodsList = async (pages) => {
  const goods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/goods?page=${pages}`
  );
  return goods.data;
};

export const getGoods = async (id) => {
  const goods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/goods/${id}`
  );
  return goods.data;
};

export const createGoods = async (formdata) => {
  const goods = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/goods`,
    formdata
  );
  return goods.data;
};

export const deleteGoods = async (id) => {
  const goods = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/goods/${id}`
  );
  return goods.data;
};

export const updateGoods = async (id, formdata) => {
  const goods = await axios.put(
    `${process.env.REACT_APP_BASE_URL}/api/v1/goods/${id}`,
    formdata
  );
  return goods.data;
};

/**
 * transaction goods services api
*/

export const getTransacGoodsList = async (pages) => {
  const transacGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/transaction-goods?page=${pages}`
  );
  return transacGoods.data;
};

export const getTransacGoods = async (id) => {
  const transacGoods = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/transaction-goods/${id}`
  );
  return transacGoods.data;
};

export const createTransacGoods = async (formdata) => {
  const transacGoods = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/transaction-goods`,
    formdata
  );
  return transacGoods.data;
};

export const deleteTransacGoods = async (id) => {
  const transacGoods = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/transaction-goods/${id}`
  );
  return transacGoods.data;
};

export const updateTransacGoods = async (id, formdata) => {
  const transacGoods = await axios.put(
    `${process.env.REACT_APP_BASE_URL}/api/v1/transaction-goods/${id}`,
    formdata
  );
  return transacGoods.data;
};