import axios from "axios";

export const getCareersList = async (pages) => {
  const careers = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers?page=${pages}`
  );
  return careers.data;
};

export const getCareer = async (params) => {
  const careers = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers/${params}`
  );
  return careers.data;
};

export const searchCareer = async (keyword) => {
  const search = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers/search?keyword=${keyword}`
  );
  return search.data;
};

export const createCareer = async (formdata) => {
  const careers = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers`,
    formdata
  );
  return careers.data;
};

export const deleteCareer = async (params) => {
  const careers = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers/${params}`
  );
  return careers.data;
};

export const updateCareer = async (id, formdata) => {
  const careers = await axios.put(
    `${process.env.REACT_APP_BASE_URL}/api/v1/careers/${id}`,
    formdata
  );
  return careers.data;
};

export const loginAdmin = async (formdata) => {
  const login = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/users/login`,
    formdata
  );
  return login.data;
};

export const registerAdmin = async (formdata) => {
  const register = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/users/signup`,
    formdata
  );
  return register.data;
};

export const getUsersDetail = async (formdata) => {
  const userDetail = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/users/detail`,
    formdata
  );
  return userDetail.data;
};

export const getUsers = async (token) => {
  const users = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/users`,
    { headers: { Authorization: `Bearer ${token}` } }
  );
  return users.data;
};

export const getCandidatesList = async (token, pages) => {
  const candidates = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/candidates?page=${pages}`,
    { headers: { Authorization: `Bearer ${token}` } }
  );
  return candidates.data;
};

export const getCandidates = async (token, id) => {
  const candidates = await axios.get(
    `${process.env.REACT_APP_BASE_URL}/api/v1/candidates/${id}`,
    { headers: { Authorization: `Bearer ${token}` } }
  );
  return candidates.data;
};

export const deleteCandidate = async (token, id) => {
  const candidates = await axios.delete(
    `${process.env.REACT_APP_BASE_URL}/api/v1/candidates/${id}`,
    { headers: { Authorization: `Bearer ${token}` } }
  );
  return candidates.data;
};

export const applyCandidates = async (formdata) => {
  const candidates = await axios.post(
    `${process.env.REACT_APP_BASE_URL}/api/v1/candidates`,
    formdata,
    {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }
  );
  return candidates.data;
};
