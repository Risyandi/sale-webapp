import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { img } from "../assets/images";

const Navbar = () => {
  let Links = [
    { name: "About", link: "/about" },
    { name: "Portofolio", link: "/portofolio" },
    { name: "Reports", link: "/reports" },
    { name: "News", link: "/news" },
    { name: "Contacts", link: "/contacts" },
  ];

  let [open, setOpen] = useState(false);

  return (
    <div className="w-screen fixed top-0 left-0 z-10">
      <div className="md:flex items-center md:justify-between py-5 bg-yellow-sale lg:px-20 px-7">
        <h1>Welcome Admin!</h1>
        <div
          onClick={() => setOpen(!open)}
          className="text-3xl text-white fixed right-8 top-6 cursor-pointer lg:hidden">
          <ion-icon name={open ? "close" : "menu"}></ion-icon>
        </div>

        <ul
          className={`lg:flex lg:items-center lg:pb-0 pb-12 absolute lg:static lg:z-auto z-[-1] left-0 w-full lg:w-auto lg:pl-0 pl-9 transition-all duration-500 ease-in ${
            open ? "top-0" : "top-[-490px]"
          }`}>
          {Links.map((link) => (
            <li key={link.name} className="lg:ml-8 text-lg lg:my-0 my-7">
              <NavLink to={link.link}>
                <div className="text-white hover:font-bold duration-100">
                  {link.name}
                </div>
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
