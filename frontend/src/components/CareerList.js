import { Link } from "react-router-dom";

const CareerList = (props) => {
  return (
    <>
      <div>
        <Link to={`careers-details/${props.id}`} className="cursor-pointer">
          <div className="flex flex-col border p-3 rounded-md hover:shadow-lg">
            <h1 className="text-xl lg:text-2xl font-semibold">{props.title}</h1>
            <div className="flex flex-row justify-between mt-2">
              <div className="flex flex-col">
                <div className="flex flex-row items-center gap-2">
                  <ion-icon name="briefcase-outline"></ion-icon>
                  <h3 className="text-sm lg:text-lg font-light">
                    {props.department}
                  </h3>
                </div>
                <div className="flex flex-row items-center gap-2">
                  <ion-icon name="location-outline"></ion-icon>
                  <h3 className="text-sm lg:text-lg font-light">
                    {props.location}
                  </h3>
                </div>
              </div>
              <div className="flex flex-row items-center gap-2">
                <ion-icon name="podium-outline"></ion-icon>
                <p className="text-sm lg:text-lg font-light">
                  {props.level_experience}
                </p>
              </div>
            </div>
          </div>
        </Link>
      </div>
    </>
  );
};

export default CareerList;
