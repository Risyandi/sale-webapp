import { Link } from "react-router-dom";
import { img } from "../assets/images";

const Header = () => {
  return (
    <>
      <div className="w-full h-96 bg-cover bg-center bg-[url('/src/assets/images/img-hero.jpg')]">
        <Link to={`/`}>
          <div className="flex flex-col justify-end items-center pb-12 w-full h-96 bg-gradient-to-br from-black-sale to-green-sale/80">
            <img
              src={img.logoNoTagline}
              alt="logo"
              className="w-36 lg:w-40"
            />
            <h1 className="text-3xl lg:text-4xl text-white font-lato">
              Sale Webapp
            </h1>
            <p className="text-md lg:text-xl font-normal text-white font-lato text-center mt-2">
            "Discover the ultimate sales solution with our Sale Webapp. Streamline your sales process and boost productivity."
            </p>
          </div>
        </Link>
      </div>
    </>
  );
};

export default Header;
