const Footer = () => {
  return (
    <>
      <div className="flex flex-col justify-center text-center items-center text-xs md:text-sm bg-yellow-sale text-black py-2">
        <p>
          Copyright © 2024 Risyandi - All
          rights reserved.
        </p>
        <p>
          Jln.Letda Lukito No16, Jatinangor - Sumedang
        </p>
      </div>
    </>
  );
};

export default Footer;
