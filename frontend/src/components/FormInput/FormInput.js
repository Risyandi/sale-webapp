const FormInput = (props) => {
  const { label, errorMessage, onChange, id, required, ...inputProps } = props;

  return (
    <div className="mb-6">
      <label className="block mb-2 text-md font-semibold text-black ">
        {label}
        {required ? <span className="text-red-600">*</span> : null}
      </label>
      <input
        {...inputProps}
        onChange={onChange}
        className="shadow-sm border border-gray-300 text-gray-900 text-md rounded-sm block w-full p-3 focus:outline-none"
      />
    </div>
  );
};

export default FormInput;
