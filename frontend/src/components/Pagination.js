import React from 'react';

const Pagination = ({ currentPage, lastPage, onNextPage, onPrevPage }) => {
    return (
        <div className="flex justify-center mt-10">
            <button
                className={`bg-green-sale hover:bg-yellow-sale hover:text-green-sale text-white font-bold py-2 px-4 rounded-l ${currentPage === 1 ? 'cursor-not-allowed opacity-50' : 'cursor-pointer'
                    }`}
                onClick={onPrevPage}
                disabled={currentPage === 1}
            >
                Previous
            </button>

            <span className="bg-gray-200 py-2 px-4">{`${currentPage} of ${lastPage}`}</span>

            <button
                className={`bg-green-sale hover:bg-yellow-sale hover:text-green-sale text-white font-bold py-2 px-4 rounded-r ${currentPage === lastPage ? 'cursor-not-allowed opacity-50' : 'cursor-pointer'
                    }`}
                onClick={onNextPage}
                disabled={currentPage === lastPage}
            >
                Next
            </button>
        </div>
    );
};

export default Pagination;
