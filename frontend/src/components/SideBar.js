import React, { useState } from "react";
import { HiMenuAlt3 } from "react-icons/hi";
import { MdAttachMoney, MdLabel, MdLeaderboard, MdWorkOutline } from "react-icons/md";
import { FiLogOut } from "react-icons/fi";
import { GrUserWorker } from "react-icons/gr";
import { Link } from "react-router-dom";
import { logout } from "../utils";

const SideBar = () => {
  const [open, setOpen] = useState(true);

  const menus = [
    { name: "Careers", link: "/admin/dashboard", icon: MdWorkOutline },
    {
      name: "Candidates",
      link: "/admin/candidates",
      icon: GrUserWorker,
    },
    { name: "Logout", link: "/", icon: FiLogOut },
    { name: "Stock", link: "/", icon: MdLeaderboard },
    { name: "Type", link: "/", icon: MdLabel },
    { name: "Transaction", link: "/", icon: MdAttachMoney },
  ];

  const clearLogout = () => {
    logout();
    setTimeout(() => {
      window.location.href = "/admin/login";
    }, 1000);
  }

  return (
    <div
      className={`bg-white min-h-screen ${open ? "w-16 lg:w-72" : "w-16"
        } duration-500 text-black px-4`}>
      <div className="py-3 justify-end hidden lg:block">
        <HiMenuAlt3
          size={26}
          className="cursor-pointer"
          onClick={() => setOpen(!open)}
        />
      </div>
      <div className="mt-2 flex flex-col gap-4 relative">
        {menus?.map((menu, i) => (
          menu.name !== 'Logout' ?
            <Link
              to={menu?.link}
              key={i}
              className={` ${menu?.margin && "mt-5"
                } group flex items-center text-sm gap-6 font-medium p-2 rounded-md`}>
              <div>{React.createElement(menu?.icon, { size: "24" })}</div>
              <h2
                style={{
                  transitionDelay: `${i + 3}00ms`,
                }}
                className={`hidden lg:block text-lg whitespace-pre duration-500 ${!open && "opacity-0 translate-x-28 overflow-hidden"
                  }`}>
                {menu?.name}
              </h2>
            </Link> :
            <div key={i} className="group flex items-center text-sm gap-6 font-medium p-2 rounded-md cursor-pointer" onClick={(e) => { clearLogout() }}>
              <div>{React.createElement(menu?.icon, { size: "24" })}</div>
              <h2
                style={{
                  transitionDelay: `${i + 3}00ms`,
                }}
                className={`hidden lg:block text-lg whitespace-pre duration-500 ${!open && "opacity-0 translate-x-28 overflow-hidden"
                  }`}>
                {menu?.name}
              </h2>
            </div>
        ))}
      </div>
    </div>
  );
};

export default SideBar;
