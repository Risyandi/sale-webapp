import logoNoTagline from "./logo_notagline.png";
import imgHero from "./img-hero.jpg";

const img = {
  logoNoTagline,
  imgHero,
};

export { img };
